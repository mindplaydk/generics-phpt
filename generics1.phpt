--TEST--
Generic class-reflection
--FILE--
<?php

class Collection<K, V>
{}

$c = new ReflectionClass(Collection::class);

var_dump($c->isGeneric());

foreach ($c->getTypeParams() as $param) {
    var_dump($param instanceof ReflectionTypeParam); 
    var_dump($param->name);
}

?>
--EXPECT--
bool(true)
bool(true)
string(1) "K"
bool(true)
string(1) "V"
