--TEST--
Upper bound reflection
--FILE--
<?php

class Feline {}

class Cat extends Feline {}

class List<T is Feline> {}

$c = new ReflectionClass(List::class);

$param = $c->getTypeParams()[0];

var_dump($param instanceof ReflectionTypeParam); 

var_dump($param->name);

$type = $param->getUpperBound();

var_dump($type instanceof ReflectionTypeHint);

var_dump($type->__toString()); // will this work??

// TODO...

?>
--EXPECT--
bool(true)
string(1) "T"
bool(true)
string(1) "Feline"
