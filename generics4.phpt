--TEST--
Generic types as type-arguments
--FILE--
<?php

class List<T>
{
    public function getItem($index) : T {
        // return ...
    }
}

class Box<T> {}

class Hat {}

$list = new List<Box<Hat>>();

$c = new ReflectionMethod(List::class);

var_dump($c->getTypeParam(0)->getClass($list)->name);

// TODO we need some way to resolve type-arguments, but is this meaningful or even possible:

var_dump($c->getTypeParam(0)->getClass($list)->getTypeParam(0)->getClass($list)->name);

?>
--EXPECT--
string(8) "Box"
string(8) "Hat"
