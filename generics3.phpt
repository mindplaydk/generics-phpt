--TEST--
Method-reflection with generic return-type
--FILE--
<?php

class Factory<T>
{
    public function create() : T
    {
        return new T();
    }
}

$m = new ReflectionMethod(Factory::class, "create");

var_dump($m->isGeneric());

$t = $m->getReturnType();

var_dump($t->isGeneric());

var_dump($t->isBuiltin());

var_dump($t->__toString());

?>
--EXPECT--
bool(true)
bool(true)
bool(false)
string(1) "T"
